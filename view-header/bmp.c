#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bmp.h"

struct BmpHeader new_header(void) {
    struct BmpHeader header = {0};
    return header;
}

struct BmpHeader init_header(struct Image const* img) {
    size_t garbage_size = 4 - (img->width * 3) % 4;

    struct BmpHeader header = new_header();
    header.bfType = 0x4d42;
    header.biSizeImage = (img->width * 3 + garbage_size) * img->height;
    header.bfileSize = header.biSizeImage + 54;
    header.bOffBits = 54;
    header.biSize = 40;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.bfReserved = 0;
    return header;
}

int read_header(FILE* in, struct BmpHeader* header) {
    return fread(header, sizeof(struct BmpHeader), 1, in);
}

enum read_status from_bmp(FILE* in, struct Image* img) {
    struct BmpHeader header = new_header();

    if (!read_header(in, &header)) {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != 0x4d42) { // BM
        return READ_INVALID_SIGNATURE;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;

    size_t garbage_size = 4 - (img->width * 3) % 4;
    uint8_t garbage [4];

    img->data = (struct Pixel*) malloc(sizeof(struct Pixel) * header.biHeight * header.biWidth);
    //for (uint32_t i = 0; i < header->biHeight; i++) {
    //    img->data[i] = (struct Pixel*) malloc(sizeof(struct Pixel) * header->biWidth);
    //}

    for (uint64_t i = 0; i < img->height * img->width; i += img->width) {
        fread(&img->data[i], sizeof(struct Pixel), img->width, in);
        fread(garbage, sizeof(uint8_t), garbage_size, in);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct Image const* img) {
    struct BmpHeader const header = init_header(img);
    
    if (fwrite(&header, sizeof(struct BmpHeader), 1, out) == 0) {
        return WRITE_ERROR;
    }

    size_t garbage_size = 4 - (img->width * 3) % 4;
    uint8_t garbage [4] = {0};

    for (uint64_t i = 0; i < img->height * img->width; i += img->width) {
        if (fwrite(&img->data[i], sizeof(struct Pixel), img->width, out) < img->width) {
            return WRITE_ERROR;
        }
        if (fwrite(garbage, sizeof(uint8_t), garbage_size, out) < garbage_size) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}