#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "image.h"
#include "bmp.h"

struct Image new_image(void) {
    struct Image img = {0};
    return img;
}

void rotate(struct Image* img) {
    struct Pixel* n_data;
    n_data = (struct Pixel*) malloc (sizeof(struct Pixel) * img->height * img->width);

    for (size_t i = 0; i < img->width; i++) {
        for (size_t j = 0; j < img->height; j++) {
            n_data[i * img->height + j] = img->data[(img->height - 1 - j) * img->width + i];
        }
    }

    uint64_t c = img->height;
    img->height = img->width;
    img->width = c;
    img->data = n_data;

    return;
}