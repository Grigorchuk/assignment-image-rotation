#include "image.h"
#include <stdio.h>

void output(const char* msg);
FILE* file_for_writing(const char* filename);
FILE* file_for_reading(const char* filename);