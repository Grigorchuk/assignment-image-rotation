#include <stdint.h>
#include <stdio.h>
#include "image.h"
#ifndef BMP_H
#define BMP_H

#pragma pack(push, 1)
struct BmpHeader {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

struct Pixel { uint8_t b, g, r; };

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

struct BmpHeader new_header(void);
int read_header(FILE* in, struct BmpHeader* header);
enum read_status from_bmp(FILE* in, struct Image* img);
enum write_status to_bmp(FILE* out, struct Image const* img );

#endif