#include <stdint.h>
#ifndef IMAGE_H
#define IMAGE_H

struct Image {
    uint64_t width, height;
    struct Pixel* data;
};

struct Image new_image(void);
void rotate(struct Image* img);

#endif