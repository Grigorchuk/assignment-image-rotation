#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "image.h"
#include "bmp.h"

void usage(void) {
    printf("Usage: ./bmp_rotate <filename_in> <filename_out>\n");
    printf("Note: filename_out is optional, by default it is equal to 'filename_in'.rotated\n");
}

int main(int argc, char** argv) {
    char *filename_in = malloc(100);
    char *filename_out = malloc(100);

    if (argc == 1) {
        usage();
        return -1;
    } else if (argc == 2) {
        strcpy(filename_in, argv[1]);
        strcpy(filename_out, "rotated_");
        strcat(filename_out, argv[1]);
    } else if (argc == 3) {
        strcpy(filename_in, argv[1]);
        strcpy(filename_out, argv[2]);   
    } else {
        output("[-] Invalid number of arguments\n");
        return -1;
    }

    output("[+] Opening file for reading\n");
    FILE* f = file_for_reading(filename_in);

    struct Image img = new_image();
    output("[+] Convert it to internal format\n");

    const enum read_status read_code = from_bmp(f, &img);
    if (read_code == READ_INVALID_SIGNATURE) {
        output("[i] Invalid signature\n");
        return -1;
    } else if (read_code == READ_INVALID_BITS) {
        output("[-] Invalid bits\n");
        return -1;
    } else if (read_code == READ_INVALID_HEADER) {
        output("[-] Invalid header\n");
        return -1;
    }

    output("[+] Rotating image\n");
    rotate(&img);

    output("[+] Opening file for writing\n");
    f = file_for_writing(filename_out);

    const enum write_status write_code = to_bmp(f, &img);
    if (write_code == WRITE_ERROR) {
        output("Error in writing to file\n");
        return -1;
    }

    output("Successfully rotated bmp image!\n");

    free(filename_in);
    free(filename_out);
    return 0;
}