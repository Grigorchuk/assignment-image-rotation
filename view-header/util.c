#include <stdlib.h>
#include "util.h"
#include "bmp.h"

void output(const char* msg) {
    fprintf(stderr, "%s", msg);
}

FILE* file_for_writing(const char* filename) {
    FILE* f;
    f = fopen(filename, "wb");
    if (f == NULL) {
        output("Error occured while opening file for reading");
        exit(-1);
    }
    return f;
}

FILE* file_for_reading(const char* filename) {
    FILE* f;
    f = fopen(filename, "rb");
    if (f == NULL) {
        output("Error occured while opening file for reading");
        exit(-1);
    }
    return f;
}